# Trainer guide for activity: encryption and end-to-end encryption

## Learning objectives:
Upon partaking in the activities, participants will be able to: 
- explain how simple encryption works [1]
- relate how encryption protects the secrecy of digital messages in transit [1,2,3]
- understand how end-to-end encryption works [3]
- make the difference between data in transit and data at rest [2,3,4]

## Material and tools needed:

### [Activity 1 and 2] Caesar cypher:
- 1 A4 printed sheet of paper per participant with following design: https://gitlab.torproject.org/raya/tangible-internet-freedom/-/blob/main/Activity:%20Encryption/printable_caesar-cipher-wheel.pdf
- 1 split paper pins
- 1 pen
- 1 pair of scissors
- Printer

### [Activity 1] Enigma:
- 1 cardboard tube
- 2 printed A4 sheets of paper per participant with following design: https://gitlab.torproject.org/raya/tangible-internet-freedom/-/blob/main/Activity:%20Encryption/printable_enigma-tube.pdf
- 1 pair of scissors
- Paper glue or clear sticky tape
- Printer

### [Activity 1] Vigenere cipher one time pad:
- 1 A4 printed sheet of paper per participant with following design: https://gitlab.torproject.org/raya/tangible-internet-freedom/-/blob/main/Activity:%20Encryption/printable_vigenere-cypher-otp.pdf
- Printer

### [Activity 2] Sending an encrypted message:
- 1 empty A4 paper
- 1 envelope
- Pens

### [Activity 3] Diffie-hellman key exchange:
- Option 1: 3D print these keys - https://www.thingiverse.com/thing:3034221/
    - 3 different colors of paint
    - 3D printer and filament
- Option 2: laser cut the same design - https://www.thingiverse.com/thing:3034221/
    - 3 different colors of paint
    - Laser cutter and think MDF wood

## Facilitation guide

### Activity 1: Encrypting and decrypting messages using symmetric encryption
Activity length: 1 hour.

- Group participants in pairs. Let participants pick one encryption method of their choice, Caeser, Vigenere, or Enigma (noting that they're sorted from least to most complex).
- One half of the pair will compose a short message and encrypt it using a key. They will proceed to share the key and cypher text with the other half of the pair.
- The other half of the pair will proceed to decrypt the message (optional: let each group time how long it takes them to encrypt and decrypt the message).
- Ask participants who decrypted the messages to stand up and share the message with the rest of the group.
- Discussion points:
    - What are different ways the secret key can be shared between pairs?

### Activity 2: Sending an encrypted message
Activity length: 45 minutes.

- Ask 5 participants to volunteer:
    - 1 participant will volunteer as the sender of an encrypted message.
    - 1 participant will volunteer as the receiver of an encrypted message.
    - 2 particpants will volunteer as each side's Internet service provider.
    - 1 participant will volunteer as the messaging application's server.
- Ask the sender to write a short letter on a piece of paper and encrypt it using Caeser cypher. The sender will place the letter inside the envelope and on the face of the envelope note down the destination of the letter (e.g. "To: Noor").
- The sender should proceed to pass the envelope to their ISP.
- Ask the sender's ISP to open the envelope and read out the text and put the letter back in the envelope. After that, the ISP should pass on the envelope to the server.
- Ask the server to open the envelope and read out the text and put the letter back in the envelope. After that, the server should pass on the envelope to the ISP of the receiver.
- Ask the receiver's ISP to open the envelope and read out the text and put the letter back in the envelope. Finally, the ISP should pass on the envelope to the end receiver.
- Ask the sender to share the secret key with the receiver by whispering it to them.
- The receiver should proceed to decrypt the letter and read it out loud.
- Discussion points:
    - Who is able to eavesdrop on the message? What capabilities do the entities in the middle have (ISPs, application servers, intelligence agencies, hackers, etc.)? Discuss traffic analysis by intelligence agencies, entities being served warrants, brute force, targeted surveillance with spyware.
    - What information are all entities able to read in plain text? Discuss metadata.
    - What are alternative ways for sharing secret keys? What happens if person A and person B can never meet to share a key in the first place?

### Activity 3: Diffie-hellman key exchange
- 5 participants volunteer as above.
- Objects representing public and private keys are handed to the sender and receiver.
- The sender and receiver perform a key exchange between them via the ISP-Server-ISP route. The key exchange is performed in the following order: https://commons.wikimedia.org/wiki/File:Diffie-Hellman_Key_Exchange.svg.
- Explain that each portion of the physical key represents a mathematical function.
- Discussion points:
    - What compromises exist with this model of key exchange? (see: https://open.oregonstate.education/app/uploads/sites/137/2021/03/dhe-mitm.png)
    - Discuss out-of-band key verifications (i.e. fingerprinting) available in messaging applications.

### Activity 4: Quiz - which apps use E2EE and which don't?
- Create a simple quiz, ask participants to raise their hands if they believe the following application uses E2EE:
	- Wire
	- Telegram
	- WhatsApp
	- Signal
	- Facebook messenger
	- Instagram direct messages
	- Twitter direct messages
	- iMessage and FaceTime
- Discussion points:
    - How can one verify that an application uses E2EE?
    - What are the privacy and security features of each application?
